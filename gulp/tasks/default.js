/****************

Create public tasks from here

****************/

var gulp        = require('gulp');
var runSequence = require('run-sequence');

gulp.task('default', function (callback) {
    runSequence('clean', 'views', 'scripts', 'styles', 'server', callback);
});



/*

Task available:
    - clean

    - html
    - styles
    - styles::reload

    - scripts ['vendors', 'browserify'], 'min-scripts', 'min-vendors' 
    - scripts::reload
    - vendors
    - vendors::reload
    - browserify

    - server ['browser-sync'], ['watch']
    - browser-sync
    - watch
    
todo:
    - tasks for jade templates
    - tasks for htaccess robots favicon
*/