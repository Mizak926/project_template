var gulp        = require('gulp');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');

// Static server
gulp.task('server', function(callback) {
    runSequence(['browser-sync'], ['watch'], callback);
});


gulp.task('browser-sync', function (callback) {
    return browserSync({
        server: {
            baseDir: "./app"
        }
    }, callback);
});

gulp.task('bs-reload', function (callback) {
    return browserSync.reload();
})
