var gulp        = require('gulp');
var path        = require('path');
var browserSync = require('browser-sync');
var runSequence = require('run-sequence');
var config      = require ('../config');


gulp.task('watch', function (callback) {

    var scripts = config.scripts + "/**/*.coffee";
    var scriptsLibs = config.scripts + "/**/*.js";

    var styles = config.styles + "/**/*.scss";

    var views = config.views + "/**/*.jade";


    gulp.watch(scripts, ['scripts::reload']);
    gulp.watch(scriptsLibs, ['vendors::reload']);
    gulp.watch(styles, ['styles::reload']);
    gulp.watch(views, ['views::reload']);



});