var config      = require('../config');
var gulp        = require('gulp');
var browserSync = require('browser-sync');
var runSequence = require('run-sequence');
var gp          = (require('gulp-load-plugins'))({ lazy: false });



gulp.task('scripts', function (callback) {
    runSequence(['vendors', 'browserify'], 'min-scripts', 'min-vendors', callback);
})

gulp.task('vendors', function () {
    return gulp.src( config.scripts + "**/*.js" )
        .pipe( gp.concat("vendors.js") )
        .pipe( gulp.dest("./tmp/") );
});


/*
MIN FOR DIST
*/

gulp.task('min-scripts', function () {
    return gulp.src('./tmp/bundle.js')
        .pipe( gulp.dest('./app/js/') );
});

gulp.task('min-vendors', function () {
    return gulp.src('./tmp/vendors.js')
        .pipe( gulp.dest('./app/js/') );
});

/*
RELOADS TASKS FOR WATCH
*/

gulp.task('scripts::reload', function () {
    runSequence('browserify', 'min-scripts', browserSync.reload);
});

gulp.task('vendors::reload', function () {
    runSequence('vendors', 'min-vendors', browserSync.reload);
});


