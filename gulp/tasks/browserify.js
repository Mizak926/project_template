var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');

var config = require('../config');
 
gulp.task('browserify', function() {

    return browserify({
        entries: ["./" + config.scripts + "main.coffee"],
        extensions: ['.coffee', '.js'],
        debug:true
    })
    .transform('coffeeify')
    .transform('deamdify')
    .transform('uglifyify')
    .bundle()
    .on('error', function(err){
        console.log(err);
        this.emit('end');
    })
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('./tmp/'));
});
