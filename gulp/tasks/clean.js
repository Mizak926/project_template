var gulp = require('gulp');
var gp   = (require('gulp-load-plugins'))({ lazy: false });


gulp.task('clean', function() {
  return gulp.src(['app', 'tmp'], {
    read: false
  }).pipe(gp.clean({
    force: true
  }));
});