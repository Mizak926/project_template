var config      = require('../config');
var gulp        = require('gulp');
var browserSync = require('browser-sync');
var runSequence = require('run-sequence');
var gp          = (require('gulp-load-plugins'))({ lazy: false });



gulp.task('views', function () {
    return gulp.src( config.views + "index.jade" )
    .pipe( gp.plumber() )
    .pipe( gp.jade({
        pretty: true,
        basedir: config.views
    }) )
    .pipe( gulp.dest('app') );
});

gulp.task('views::reload', function () {
    runSequence('views', browserSync.reload);
});