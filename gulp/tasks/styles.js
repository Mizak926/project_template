var config      = require('../config');
var gulp        = require('gulp');
var browserSync = require('browser-sync');
var runSequence = require('run-sequence');
var gp          = (require('gulp-load-plugins'))({ lazy: false });


gulp.task('styles', function () {
    return gp.rubySass( config.styles, {
        compass: true,
        style: 'compressed',
        sourcemap: true
    })
    .on( 'error', function (err) {
        
        console.log('SASS ERROR', err);
    })
    .pipe( gp.cssmin({
            keepSpecialComments: 0
        }) 
    )
    .pipe( gp.autoprefixer( 'last 1 version' ) )
    .pipe( gp.rename({ 
        suffix: '.min'
        })
    )
    .pipe( gp.sourcemaps.write() )
    .pipe( gulp.dest('./app/css/') );
});

gulp.task('styles::reload', function () {
    runSequence('styles', browserSync.reload);
});