module.exports = {
    main:           'src/',
    styles:         'src/styles/',
    images:         'src/images/',
    scripts:        'src/scripts/',
    templates:      'src/templates/',
    views:          'src/views/'
}